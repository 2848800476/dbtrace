package john.walker.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import john.walker.log.ConsoleLogger;
import john.walker.log.ILog;
import john.walker.log.LogFactory;
import john.walker.log.MultiLogger;
import us.codecraft.xsoup.Xsoup;

/**
 * @author Johnnie Walker
 * @date 2017年4月10日
 *
 */
public class XmlFileLoader {

	public static String CONFIG_FILE_NAME = "dbtrace.xml";

	private static Document document = null;

	private XmlFileLoader () {

	}

	public static void main(String[] args) {
		System.out.println();
	}

	static {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream in = null;
		try {
			in = loader.getResourceAsStream(CONFIG_FILE_NAME);
			document = Jsoup.parse(in, "UTF-8", "");
			System.out.println(loadFilterWords());
		} catch (IOException e) {
			StringWriter writer = new StringWriter();
			e.printStackTrace(new PrintWriter(writer));
			LogFactory.getLogger().log(writer.toString());
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 加载过滤器
	 *
	 * @return
	 */
	public static List<String> loadFilterWords() {
		Elements elements = Xsoup.select(document, "//monitor/excludes/exclude").getElements();
		List<String> words = new ArrayList<String>(elements.size());
		for(Element element : elements) {
			words.add(element.text().trim());
		}
		return words;
	}

	/**
	 * 加载日志组件
	 */
	public static void loadLoggers() {
		Elements elements = Xsoup.select(document, "//monitor/loggers/logger").getElements();
		List<String> loggers = new ArrayList<String>(elements.size());
		for(Element element : elements) {
			loggers.add(element.text().trim());
		}
		MultiLogger mutiLogger = new MultiLogger();
		boolean hasOne = false;
		for(String loggerClassName : loggers) {
			try {
				ILog logger = (ILog) Class.forName(loggerClassName).newInstance();
				mutiLogger.addLogger(logger);
				hasOne = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(!hasOne) {
			LogFactory.setLogger(new ConsoleLogger());
		} else {
			LogFactory.setLogger(mutiLogger);
		}
	}

	/**
	 * sql 监控开关
	 *
	 * @return
	 */
	public static boolean sqlMonitor() {
		Elements elements = Xsoup.select(document, "//monitor/queryLog").getElements();
		if(elements == null) {
			return false;
		}

		return Boolean.valueOf(elements.get(0).text());
	}

	/**
	 * sql监控调用栈开关
	 *
	 * @return
	 */
	public static boolean sqlMonitorStack() {
		Elements elements = Xsoup.select(document, "//monitor/queryLogTrace").getElements();
		if(elements == null) {
			return false;
		}

		return Boolean.valueOf(elements.get(0).text());
	}

	/**
	 * sql监控时间
	 *
	 * @return
	 */
	public static long sqlMonitorTime() {
		Elements elements = Xsoup.select(document, "//monitor/queryLogThreshold").getElements();
		if(elements == null) {
			return 0;
		}
		try {
			return Long.valueOf(elements.get(0).text());
		} catch (Exception e) {
			return 0;
		}
	}
}
