package john.walker.log;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Johnnie Walker
 *
 */
public class MultiLogger extends AbstractLogger {

	private List<ILog> loggers = new ArrayList<ILog>();

	public MultiLogger() {

	}

	public MultiLogger(ILog logger) {
		if (logger != null) {
			this.loggers.add(logger);
		}
	}

	public void addLogger(ILog logger) {
		if (logger == null) {
			return;
		}
		loggers.add(logger);
	}


	@Override
	public void debug(String msg) {
		for (ILog logger : loggers) {
			logger.debug(msg);
		}
	}


	@Override
	public void info(String msg) {
		for (ILog logger : loggers) {
			logger.info(msg);
		}
	}


	@Override
	public void warn(String msg) {
		for (ILog logger : loggers) {
			logger.warn(msg);
		}
	}


	@Override
	public void error(String msg) {
		for (ILog logger : loggers) {
			logger.error(msg);
		}
	}


	@Override
	public void fatal(String msg) {
		for (ILog logger : loggers) {
			logger.fatal(msg);
		}
	}


	@Override
	public void newLine() {
		for (ILog logger : loggers) {
			logger.newLine();
		}
	}

}
